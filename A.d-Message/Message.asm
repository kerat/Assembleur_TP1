.386
.model flat,stdcall
option casemap:none

include d:\masm32\include\windows.inc
include d:\masm32\include\user32.inc
include d:\masm32\include\kernel32.inc
includelib d:\masm32\lib\kernel32.lib
includelib d:\masm32\lib\user32.lib

.DATA
; variables initialisees
MsgBoxCaption  db	"Hello",0
MsgBoxText     db  "Hello World!",10,0

.DATA?
; variables non-initialisees (bss)

.CODE
start:
		push MB_OK
		push offset MsgBoxCaption
		push offset MsgBoxText
		push NULL
		call MessageBox
		mov eax, 0
	    invoke	ExitProcess,eax

end start

