.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc
include factorielle.asm

.DATA
message_entree db "Entrer un entier positif : ",10,0
format_s db "%d",0
message_resultat db "La factorielle de %d est : %d",10,0
strCommand db "Pause",13,10,0

.DATA?
n DWORD ?

.CODE
start:
	push offset message_entree
	call crt_printf
	add esp, 4
	
	push offset n
	push offset format_s
	call crt_scanf
	add esp, 8
	
	push n
	call factorielle
	add esp, 4
	
	push eax
	push n
	push offset message_resultat
	call crt_printf
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke ExitProcess,eax
end start