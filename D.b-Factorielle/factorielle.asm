.DATA
sign_s db "L'argument est negatif !",10,0
over_s db "La valeur est trop grande pour du 32 bits !",10,0

.CODE
; Renvoie la factorielle du nombre entier positif donné en argument
factorielle PROC
	push ebp
	mov ebp, esp
	
	mov ecx, [ebp+8]
	
	cmp ecx, 0
	je exit_0
	
	cmp ecx, 0
	jl sign_error
	
	sub ecx,1
	push ecx
	call factorielle
	pop ecx
	add ecx, 1
	mul ecx
	jo overflow_error
	pop ebp
	ret
	
	exit_0:
		mov eax, 1
		pop ebp
		ret
	sign_error:
		push offset sign_s
		call crt_printf
		add esp, 4
		jmp exit_error
	overflow_error:
		push offset over_s
		call crt_printf
		add esp, 4
	exit_error:
		mov eax, 1
		pop ebp
		ret
factorielle ENDP