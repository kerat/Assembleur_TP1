.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc
include compte.asm

.DATA
chaine     db "aaaabbbccd",0
message    db "Chaine : %s",10,0
resultat   db "Resultats :",10,"a : %d",10,"b : %d",10,"c : %d",10,0
strCommand db "Pause",13,10,0

.CODE
start:
	push offset chaine
	push offset message
	call crt_printf
	
	push offset chaine
	call compte
	
	push ecx
	push ebx
	push eax
	push offset resultat
	call crt_printf
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke ExitProcess,eax
	
end start