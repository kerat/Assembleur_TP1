.CODE
; Compte a, b et c
compte PROC
	push ebp                            ; On sauvegarde ebp sur la pile
	mov ebp, esp                        ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov esi, [ebp+8]                    ; Adresse de la chaine dans esi
	
	; On initialise nos compteur
	mov eax, 0d                   ; a = 0
	mov ebx, 0d                   ; b = 0
	mov ecx, 0d                   ; c = 0
	
	debut_boucle:
	    cmp byte ptr [esi], 0                    ; Si on est à la fin de la chaine...
		je retour                       ; On retourne
		
		cmp byte ptr [esi], 97d                  ; Si la lettre est a...
		je plus_a                       ; On saute à notre routine d'incrementation
		
		cmp byte ptr [esi], 98d                  ; Si la lettre est b...
		je plus_b                       ; On saute à notre routine d'incrementation
		
		cmp byte ptr [esi], 99d                  ; Si la lettre est c...
		je plus_c                       ; On saute à notre routine d'incrementation
		
		; Si le caractère n'est ni a ni b ni c on ne fait rien
		inc_chaine:
			add esi, 1
		
		jmp debut_boucle                ; On boucle
		
	plus_a:
		add eax, 1d                     ; On incremente notre compteur de a
		jmp inc_chaine                  ; On reprends la boucle
	
	plus_b:
		add ebx, 1d                     ; On incremente notre compteur de b
		jmp inc_chaine                  ; On reprends la boucle
		
	plus_c:
		add ecx, 1d                     ; On incremente notre compteur de c
		jmp inc_chaine                  ; On reprends la boucle
	
	retour:
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
compte ENDP
