.CODE
; Suite de Fibonacci
myst PROC
	push ebp                           ; On sauvegarde ebp sur la pile
	mov ebp, esp                       ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov ecx, [ebp+8]                   ; On recupere l'argument
	
	sub esp, 16d                        ; On reserve 16 octets sur la pile (4 int)
	
	; On initalise les variables
	mov DWORD PTR [ebp-4], 3d           ; i=3
	mov DWORD PTR [ebp-8], 1d           ; j=1
	mov DWORD PTR [ebp-12], 1d          ; k=1
	mov DWORD PTR [ebp-16], 0           ; l=0
	
	debut_boucle:
	    cmp [ebp-4], ecx                ; Si i > n (arg1)...
		jg retour                       ; On entre pas dans la boucle
		
		mov eax, [ebp-8]                ; eax=j
		add eax, [ebp-12]               ; eax=j+k
		mov [ebp-16], eax               ; l=j+k
		
		mov ebx, [ebp-12]               ; j=k
		mov [ebp-8], ebx                ; j=k
		
		mov ebx, [ebp-16]               ; k=l
		mov [ebp-12], ebx               ; k=l
		
		add DWORD PTR [ebp-4], 1d       ; i++
		
		jmp debut_boucle                ; On boucle
	
	retour:
		mov eax, [ebp-12]               ; On retourne k (eax = valeur retournée)
		add esp, 16d                    ; On nettoie la pile
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
myst ENDP
