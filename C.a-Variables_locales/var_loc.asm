.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc
include myst.asm

.DATA
n          dd 4
message    db "N = %d",10,0
resultat   db "Resultat : %d",10,0
strCommand db "Pause",13,10,0

.CODE
start:
	push n
	push offset message
	call crt_printf
	
	push n
	call myst ; Fibonacci
	
	push eax
	push offset resultat
	call crt_printf
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke ExitProcess,eax
	
end start