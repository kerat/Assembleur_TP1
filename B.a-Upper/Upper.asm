.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\gdi32.inc
include c:\masm32\include\windows.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Phrase     db    "HELLO World",10,0
strCommand db "Pause",13,10,0

.CODE
start:
	mov ebx, offset Phrase
	
	upper:
		; On va chercher la lettre pointé par ebx et on la met dans dl
		mov dl, byte ptr [ebx]
		; On vérifie que l'on est pas à la fin de la chaine
		cmp dl, 0
		; Si oui on affiche le resultat
		jz affiche

		; On verifie qu'on est bien sur une lettre minuscule,
		; sinon on saute directement à la fin :
		cmp dl, 96
		jl end_upper
		cmp dl, 123
		jg end_upper

		; On met la lettre dans edx en maj. (décalage de -32 dans la table ASCII)	
		sub edx, 32
		; On écrase l'ancienne lettre dans la chaine par la nouvelle
		mov byte ptr [ebx], dl
		
		end_upper:
			; On incrémente notre pointeur de chaine de 1
			add ebx, 1
			; On recommence...
			jmp upper
	
	affiche:
		; on met phrase sur la pile
		push offset Phrase
		; call printf
		call crt_printf
		
		invoke crt_system, offset strCommand
		mov eax, 0
		invoke	ExitProcess,eax
		
end start

