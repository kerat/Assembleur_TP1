.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc
include diviseurs.asm

.DATA
message_entree db "Entrer un entier positif : ",10,0
format_s db "%d",0
message_confirmation db "Les diviseurs de %d sont :",10,0
strCommand db "Pause",13,10,0

.DATA?
n DWORD ?

.CODE
start:
	push offset message_entree
	call crt_printf
	
	push offset n
	push offset format_s
	call crt_scanf
	
	push n
	push offset message_confirmation
	call crt_printf
	
	push n
	call diviseurs
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke ExitProcess,eax
end start