.DATA
sign_s db "L'argument est negatif !",10,0
divise_s db "%d ",0
new_line_s db " ",10,0

.CODE
; Affiche tous les diviseurs d'un entier positif
; donne en argument
diviseurs PROC
	push ebp
	mov ebp, esp
	mov esi, [ebp+8]
	
	cmp esi, 0
	jl sign_error
	
	mov ecx, 1
	boucle_debut:
		mov eax, esi
		mov edx, 0
		div cx
		cmp edx, 0
		je divise
	
	boucle_fin:
		cmp ecx, esi
		je exit_0
		add ecx, 1
		jmp boucle_debut
	
	divise:
		push ecx
		push offset divise_s
		call crt_printf
		mov ecx, [esp+4]
		add esp, 8
		jmp boucle_fin
	
	exit_0:
		push offset new_line_s
		call crt_printf
		add esp, 4
		mov eax, 0
		pop ebp
		ret
		
	sign_error:
		push offset sign_s
		call crt_printf
		add esp, 4
	exit_error:
		mov eax, 1
		pop ebp
		ret
diviseurs ENDP