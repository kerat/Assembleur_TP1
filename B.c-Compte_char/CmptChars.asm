;.386
;.model flat,stdcall
;option casemap:none

.CODE
;start:
cmpt_chars PROC
    ; On sauvegarde ebp
	push ebp
	; On copie la pile dans ebp
	mov ebp, esp
	; On copie l'argument dans ebx
	mov ebx, [ebp+8]
	; On initialise notre compteur
	mov ecx, 0
	begin:
		; On va chercher la lettre pointé par ebx et on la met dans dl
		mov dl, byte ptr [ebx]
		; On vérifie que l'on est pas à la fin de la chaine
		cmp dl, 0
		; Si oui on affiche le resultat
		jz return

		; On incrémente notre compteur
		add ecx, 1
		
		end_cmpt:
			; On incrémente notre pointeur de chaine de 1
			add ebx, 1
			; On recommence...
			jmp begin
		return:
		    mov eax, ecx
			pop ebp
			ret
cmpt_chars ENDP
;end start

