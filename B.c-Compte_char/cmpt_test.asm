.386
.model flat,stdcall
option casemap:none

include c:\masm32\include\gdi32.inc
include c:\masm32\include\windows.inc
include c:\masm32\include\gdiplus.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc
; On inclus notre procedure compteur chars
include CmptChars.asm

includelib c:\masm32\lib\gdi32.lib
includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Phrase     db    "Bonjour",0
Message    db    "Nombre de caracteres : %d",10,0
strCommand db "Pause",13,10,0

.CODE
start:
		; on met phrase sur la pile
		push offset Phrase
		; On appelle la procedure upper
		call cmpt_chars
		; On met le resultat de cmpt_chars sur la pile
		push eax
		; On met Message sur la pile
		push offset Message
		; call printf
		call crt_printf
		
		invoke crt_system, offset strCommand
		mov eax, 0
		invoke	ExitProcess,eax
		
end start

