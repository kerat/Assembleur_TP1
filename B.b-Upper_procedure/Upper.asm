.CODE
upper PROC
	push ebp
	mov ebp, esp
	mov ebx, [ebp+8]
	begin:
		; On va chercher la lettre pointé par ebx et on la met dans dl
		mov dl, byte ptr [ebx]
		; On vérifie que l'on est pas à la fin de la chaine
		cmp dl, 0
		; Si oui on affiche le resultat
		jz return

		; On verifie qu'on est bien sur une lettre minuscule,
		; sinon on saute directement à la fin :
		cmp dl, 96
		jl end_upper
		cmp dl, 123
		jg end_upper

		; On met la lettre dans edx en maj. (décalage de -32 dans la table ASCII)	
		sub edx, 32
		; On écrase l'ancienne lettre dans la chaine par la nouvelle
		mov byte ptr [ebx], dl
		
		end_upper:
			; On incrémente notre pointeur de chaine de 1
			add ebx, 1
			; On recommence...
			jmp begin
		return:
			pop ebp
			ret
upper ENDP
